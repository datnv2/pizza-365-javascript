$(document).ready(function() { // $(document).ready(function() start
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365";
    const gBASE_VOUCHER_URL = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/";
    const gUTF8_TEXT_APPLICATION_HEADER = "application/json;charset=UTF-8";
    const TEST = "no";
    // bạn có thê dùng để lưu trữ combo được chọn, mỗi khi khách chọn bạn lại đổi giá trị properties của nó
    var gSelectedMenuStructure = {
            menuName: "", // S, M, L
            duongKinhCM: 0,
            suongNuong: 0,
            saladGr: 0,
            drink: 0,
            priceVND: 0
        } // var gSelectedMenuStructure end

    var gSelectedMenuStructureS = {
        menuName: "Small",
        duongKinhCM: "10cm",
        suongNuong: "2",
        saladGr: "200g",
        drink: "2",
        priceVND: "150000"
    };

    var gSelectedMenuStructureM = {
        menuName: "Medium",
        duongKinhCM: "25cm",
        suongNuong: "4",
        saladGr: "300g",
        drink: "3",
        priceVND: "200000"
    };

    var gSelectedMenuStructureL = {
        menuName: "Large",
        duongKinhCM: "30cm",
        suongNuong: "8",
        saladGr: "500g",
        drink: "4",
        priceVND: "300000"
    };

    // khai báo biến toàn cục các phần tử của order
    var gOrder = {
            fullname: "",
            email: "",
            phoneNumber: "",
            address: "",
            message: "",
            voucher: "",
            percentDiscount: "",
            total: "",
        } //var gOrder end
        // khai báo biến toàn cục các phần tử của Drink list
    var gSelectedDrink = {
        "value": "",
        "text": ""
    };
    // khai báo biến toàn cục biến Pizzatype
    var gSelectedPizzaType = "";
    // khai báo biến toàn cục các phần tử của NewOrder
    var gNewOrderObject = {
        id: "",
        orderId: "",
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: "",
        hoTen: "",
        thanhTien: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: "",
        ngayTao: "",
        ngayCapNhat: "",
    };
    // khai báo biến  Lời cảm ơn sau khi hoàn tất đơn hàng
    var gThankYou = "Cảm ơn bạn đã đặt hàng tại Piza 365. Mã đơn hàng của bạn là";
    // khai báo đường dẫn cho page theo dõi đơn hàng
    const gVIEW_ORDER_URL = "viewOrder.html"
        /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoad();

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // function onPageLoad - thực hiện khi bắt đầu tải trang
    function onPageLoad() {
        "use strict";
        adminTest();
        callAPIDrinkList();
    } // function onPageLoad end
    // function call Api lấy drink list
    function callAPIDrinkList() {
        "use strict";
        $.ajax({
            url: gBASE_URL + "/drinks",
            type: 'GET',
            dataType: 'json',
            success: function(responseDrink) {
                handleDrinkList(responseDrink);
            },
            error: function(ajaxContext) {
                alert(ajaxContext.responseText)
            }
        });
    } // function call Api lấy drink list end

    // Chức năng Nhập sẵn dữ liệu khi test code - ẩn đi chạy thực tế //
    function adminTest() {
        "use strict";
        if (TEST == "yes") {
            $("#inp-fullname").val("Cao Việt Bách");
            $("#inp-email").val("backcao@gmail.com");
            $("#inp-fone").val("0706789291");
            $("#inp-address").val("Đà Lạt");
            $("#inp-message").val("Giờ hành chính");
            $("#inp-voucher").val("95531");
        } else {
            $("#inp-fullname").val("");
            $("#inp-email").val("");
            $("#inp-fone").val("");
            $("#inp-address").val("");
            $("#inp-message").val("");
            $("#inp-voucher").val("");
        }
    } // Chức năng Nhập sẵn dữ liệu khi test code - ẩn đi chạy thực tế end // 

    // function thêm các option trong slection drink list từ responsive api trả về
    function handleDrinkList(paramSoftDrinkObj) {
        "use strict";
        // tạo 1 tùy chọn (option) mặc định
        $("#select-drinks").append(new Option("Vui Lòng chọn Nước Uống", ""));
        // Duyệt mảng chưa toàn bộ phần tử con của vDrinkObjects,
        for (var bI = 0; bI < paramSoftDrinkObj.length; bI++) {
            // tạo 1 tùy chọn (option) tương ứng với 1 phần tử duyệt thấy
            // gán giá trị ( value và text ) cho phần tử tìm được
            $("#select-drinks").append(
                new Option(paramSoftDrinkObj[bI].tenNuocUong), paramSoftDrinkObj[bI].maNuocUong);
            // thêm các phần tử con của paramSoftDrinkObj vào tùy chọn ( option vừa tạo ở trên )
        }
    } //function handleDrinkList end

    // function xử lý khi chọn thức uống
    $("#select-drinks").on('change', function() {
        // Xuất ra console.log các giá trị thay đổi khi click vào các button chọn Size Pizza
        console.log("--------------------------------------------------------"); // line end
        console.log("selected Drink: " + $("#select-drinks option:selected").val()); //tracking
        console.log("--------------------------------------------------------"); // line end
    }); // function xử lý khi chọn thức uống end

    // function xử lý khi click vào menu S
    $("#btn-sizeS").on("click", function() {
        onBtnSizeClick("Small", "10cm", "2", "200g", "2", "150000");
    });
    // function xử lý khi click vào menu M
    $("#btn-sizeM").on("click", function() {
        // gSelectedMenuStructure = gSelectedMenuStructureM,
        onBtnSizeClick("Medium", "25cm", "4", "300g", "3", "200000");
    });
    // function xử lý khi click vào menu L 
    $("#btn-sizeL").on("click", function() {
        // gSelectedMenuStructure = gSelectedMenuStructureL,
        onBtnSizeClick("Large", "30cm", "8", "500g", "4", "300000");
    });

    // function xử lý khi click vào menu theo dõi đơn hàng
    $("#btn-view-order-page").on("click", function() {
        window.location.href = gVIEW_ORDER_URL + "?id=" + gNewOrderObject.id + "&orderid=" + gNewOrderObject.orderId;
    }); // function xử lý khi click vào menu theo dõi đơn hàng end
    // function xử lý khi click vào menu theo dõi đơn hàng
    $("#btn-view-order-page-head").on("click", function() {
        window.location.href = gVIEW_ORDER_URL + "?id=" + gNewOrderObject.id + "&orderid=" + gNewOrderObject.orderId;
    }); // function xử lý khi click vào menu theo dõi đơn hàng end


    // function onBtnSizeClick - chức năng đổi màu button khi click vào chọn Size pizza
    function onBtnSizeClick(paraMenuName, paraDiameter, paraBakedRibs, paraSalad, paraSoftDrink, paraPrice) {
        "use strict";
        // truyền vào giá trị cho biến toàn cục "gSelectedMenuStructure.menuName" theo khai báo từ HTML
        gSelectedMenuStructure.menuName = paraMenuName;
        //set trạng thái màu button - set the buttons size 
        changeInfoSizePizza(paraMenuName);
        // truyền vào các giá trị cho biến toàn cục "gSelectedMenuStructure" theo khai báo từ HTML
        gSelectedMenuStructure.duongKinhCM = parseInt(paraDiameter);
        gSelectedMenuStructure.suongNuong = parseInt(paraBakedRibs);
        gSelectedMenuStructure.saladGr = parseInt(paraSalad);
        gSelectedMenuStructure.drink = parseInt(paraSoftDrink);
        gSelectedMenuStructure.priceVND = parseInt(paraPrice)

        // Xuất ra console.log các giá trị thay đổi khi click vào các button chọn Size Pizza
        console.log("--------------------------------------------------------"); // line end
        console.log("selected Pizza Size: " + gSelectedMenuStructure.menuName); //tracking
        console.log("duongKinhCM: " + gSelectedMenuStructure.duongKinhCM); //tracking
        console.log("suongNuong: " + gSelectedMenuStructure.suongNuong); //tracking
        console.log("saladGr: " + gSelectedMenuStructure.saladGr); //tracking
        console.log("drink: " + gSelectedMenuStructure.drink); //tracking
        console.log("priceVND: " + gSelectedMenuStructure.priceVND); //tracking
        console.log("--------------------------------------------------------"); // line end
    } // function onBtnSizeClick - chức năng đổi màu button khi click vào chọn Size pizza end


    // function switch - set classname cho button khi click vào chọn size ( Small, Medium, Large )
    function changeInfoSizePizza(paraMenuName, paraDiameter, paraBakedRibs, paraSalad, paraSoftDrink, paraPrice) {
        "use strict";
        // switch start 
        switch (paraMenuName) {
            case "Small":
                {
                    // đổi chữ "đã chọn" cho button size S
                    $("#btn-sizeS").html("Đã Chọn").removeClass('btn-outline-secondary text-danger').addClass("btn-outline-danger btn-danger text-white"); // S
                    $("#btn-sizeM").html("Chọn").removeClass('btn-outline-danger btn-danger text-white').addClass("btn-outline-secondary"); // M
                    $("#btn-sizeL").html("Chọn").removeClass('btn-outline-danger btn-danger text-white').addClass("btn-outline-secondary"); // L
                    // đổi màu cho background top của tùy chọn size S
                    $("#divS").removeClass('text-black-50').addClass("text-danger btn-danger"); // S
                    $("#divM").removeClass('text-danger btn-danger').addClass("text-black-50"); // M
                    $("#divL").removeClass('text-danger btn-danger').addClass("text-black-50"); // L
                    break;
                }
            case "Medium":
                {
                    // đổi chữ "đã chọn" cho button size M
                    $("#btn-sizeS").html("Chọn").removeClass('btn-outline-danger btn-danger text-white').addClass("btn-outline-secondary"); // S
                    $("#btn-sizeM").html("Đã Chọn").removeClass('btn-outline-secondary text-danger').addClass("btn-outline-danger btn-danger text-white"); // M
                    $("#btn-sizeL").html("Chọn").removeClass('btn-outline-danger btn-danger text-white').addClass("btn-outline-secondary"); // L
                    // đổi màu cho background top của tùy chọn size M
                    $("#divS").removeClass('text-danger btn-danger').addClass("text-black-50"); // S
                    $("#divM").removeClass('text-black-50').addClass("text-danger btn-danger"); // M
                    $("#divL").removeClass('text-danger btn-danger').addClass("text-black-50"); // L
                    // đổi màu cho thẻ div của tùy chọn size M
                    break;
                }
            case "Large":
                {
                    // đổi chữ "đã chọn" cho button size L
                    $("#btn-sizeS").html("Chọn").removeClass('btn-outline-danger btn-danger text-white').addClass("btn-outline-secondary"); // S
                    $("#btn-sizeM").html("Chọn").removeClass('btn-outline-danger btn-danger text-white').addClass("btn-outline-secondary"); // M
                    $("#btn-sizeL").html("Đã Chọn").removeClass('btn-outline-secondary text-danger').addClass("btn-outline-danger btn-danger text-white"); // L
                    // đổi màu cho background top của tùy chọn size M
                    $("#divS").removeClass('text-danger btn-danger').addClass("text-black-50"); // S
                    $("#divM").removeClass('text-danger btn-danger').addClass("text-black-50"); // M
                    $("#divL").removeClass('text-black-50').addClass("text-danger btn-danger"); // L
                    break;
                }
            default:
                {}
        }
        // switch end 
    } // function switch - set classname cho button khi click vào chọn size ( Small, Medium, Large ) end

    // function xử lý khi click vào menu Hawai
    $("#btn-hawai").on("click", function() {
        onBtnTypeClick("hawai");
    });
    // function xử lý khi click vào menu Seafood
    $("#btn-seafood").on("click", function() {
        onBtnTypeClick("seafood");
    });
    // function xử lý khi click vào menu Bacon
    $("#btn-bacon").on("click", function() {
        onBtnTypeClick("Bacon");
    });

    // function onBtnTypeClick - Hàm đổi màu button khi click vào chọn type pizza
    function onBtnTypeClick(paraType) {
        "use strict";
        // // Bước 1: Truy xuất dữ liệu và gán cho biến gSelectedPizzaType
        gSelectedPizzaType = paraType;
        // Bước 2: Validate dữ liệu - pass
        // Bước 3: Thực hiện xử lý dữ liệu - set trạng thái của nut - set the buttons style 
        changeInfoTypePizza(gSelectedPizzaType);
        // Xuất ra console.log các giá trị thay đổi khi click vào các button chọn Size Pizza
        console.log("--------------------------------------------------------"); // line end
        console.log("selected PizzaType: " + gSelectedPizzaType); //tracking
        console.log("--------------------------------------------------------"); // line end
    } // function onBtnTypeClick end

    // function switch - set classname cho button khi click vào chọn size ( hawai, seafood, bacon )
    function changeInfoTypePizza(paramTypePizza) {
        "use strict";
        // switch start 
        switch (paramTypePizza) {
            case "hawai":
                {
                    // đổi chữ "đã chọn" cho button type hawai
                    $("#btn-hawai").html("Đã Chọn").removeClass('btn-outline-secondary').addClass("btn btn-lg btn-block btn-outline-danger btn-danger text-white"); // Hawai
                    $("#btn-seafood").html("Chọn").removeClass('btn-outline-danger btn-danger text-white').addClass("btn btn-lg btn-block btn-outline-secondary"); // Seafood
                    $("#btn-bacon").html("Chọn").removeClass('btn-outline-danger btn-danger text-white').addClass("btn btn-lg btn-block btn-outline-secondary"); // Bacon

                    // đổi className cho button type hawai
                    $("#div-hawai").removeClass('text-black-50').addClass("card-body text-danger");
                    $("#div-seafood").removeClass('text-danger btn-danger').addClass("card-body text-black-50");
                    $("#div-bacon").removeClass('text-danger btn-danger').addClass("card-body text-black-50");
                    break;
                }
            case "seafood":
                {
                    // đổi chữ "đã chọn" cho button type seafood
                    $("#btn-hawai").html("Chọn").removeClass('btn-outline-danger btn-danger text-white').addClass("btn btn-lg btn-block btn-outline-secondary"); // Hawai
                    $("#btn-seafood").html("Đã Chọn").removeClass('btn-outline-secondary').addClass("btn btn-lg btn-block btn-outline-danger btn-danger text-white"); // Seafood
                    $("#btn-bacon").html("Chọn").removeClass('btn-outline-danger btn-danger text-white').addClass("btn btn-lg btn-block btn-outline-secondary"); // Bacon
                    // đổi className cho button type seafood
                    $("#div-hawai").removeClass('text-danger btn-danger').addClass("card-body text-black-50");
                    $("#div-seafood").removeClass('text-black-50').addClass("card-body text-danger");
                    $("#div-bacon").removeClass('text-danger btn-danger').addClass("card-body text-black-50");
                    break;
                }
            case "Bacon":
                {
                    // đổi chữ "đã chọn" cho button type bacon
                    $("#btn-hawai").html("Chọn").removeClass('btn-outline-danger btn-danger text-white').addClass("btn btn-lg btn-block btn-outline-secondary"); // Hawai
                    $("#btn-seafood").html("Chọn").removeClass('btn-outline-danger btn-danger text-white').addClass("btn btn-lg btn-block btn-outline-secondary"); // Seafood
                    $("#btn-bacon").html("Đã Chọn").removeClass('btn-outline-secondary').addClass("btn btn-lg btn-block btn-outline-danger btn-danger text-white"); // Bacon
                    // đổi className cho button type seafood
                    $("#div-hawai").removeClass('text-danger btn-danger').addClass("card-body text-black-50");
                    $("#div-seafood").removeClass('text-danger btn-danger').addClass("card-body text-black-50");
                    $("#div-bacon").removeClass('text-black-50').addClass("card-body text-danger");
                    break;
                }
            default:
                {
                    // mặc định cho button type 
                    $("#btn-hawai").html("Chọn").addClass("btn btn-lg btn-block btn-outline-secondary"); // Hawai
                    $("#btn-seafood").html("Chọn").addClass("btn btn-lg btn-block btn-outline-secondary"); // Seafood
                    $("#btn-bacon").html("Chọn").addClass("btn btn-lg btn-block btn-outline-secondary"); // Bacon
                    // đổi className cho button type seafood
                    $("#div-hawai").addClass("card-body text-black");
                    $("#div-seafood").addClass("card-body text-black");
                    $("#div-bacon").addClass("card-body text-black");
                }
        }
        // switch end 
    } // function switch - set classname cho button khi click vào chọn size ( hawai, seafood, bacon ) end


    // function onBtnSendFormClick 
    // input: gOrder,gSelectedMenuStructure
    // output: gOrder, gSelectedMenuStructure, gSelectedPizzaType   
    $("#btn-send-form").on("click", function() {
        "use strict";
        // khai báo đối tượng gOrder chứa dữ liệu
        // B1: thu thập dữ liệu
        getDataOrder();
        // B2: kiểm tra dữ liệu (validate dữ liệu)
        var vIsValidate = validateOrder(gOrder);
        if (vIsValidate == true) {
            // Bước 3: Call API - get Voucher Code API
            loadAjaxAPIGetVoucher();
        }
    }); // function onBtnSendFormClick end

    // hàm truy xuất các dữ liệu nhập vào của người dùng
    function getDataOrder() {
        "use strict";
        // 1 truy xuất phần tử
        // đọc giá trị và lưu vào đối tượng
        gOrder.fullname = $("#inp-fullname").val().trim();
        gOrder.email = $("#inp-email").val().trim();
        gOrder.fone = $("#inp-fone").val().trim();
        gOrder.address = $("#inp-address").val().trim();
        gOrder.message = $("#inp-message").val().trim();
        gOrder.voucher = $("#inp-voucher").val().trim();

        // lấy giá trị nước uống được chọn
        gSelectedDrink.value = $("#select-drinks option:selected").index();
        gSelectedDrink.text = $("#select-drinks option:selected").text();

    } // function getDataOrder end

    // function validate dữ liệu
    // input: đối tượng dữ liệu thông tin người dùng nhập vào
    // output: trả về true nếu tất cả dữ liệu hợp lệ; ngược lại trả về false nếu có 1 dữ liệu không hợp lệ ( không nhập)
    function validateOrder(paramOrderObj) {
        "use strict";
        var vResult = true;
        var vMessage = "";

        //Check Combo Pizza Empty
        if (gSelectedMenuStructure.menuName === "") {
            vResult = false;
            vMessage += "Please Select Combo Pizza !" + "\n";
        }
        //Check PizzaType Empty
        if (gSelectedPizzaType === "") {
            vResult = false;
            vMessage += "Please Select PizzaType !" + "\n";
        }
        //Check Drink type 
        if (gSelectedDrink.text === "Vui Lòng chọn Nước Uống") {
            vResult = false;
            vMessage += "Please Select Drink Type !" + "\n";
        }
        //Check fullname Empty
        if (paramOrderObj.fullname === "") {
            vResult = false;
            vMessage += "Please type Full Name !" + "\n";
        }
        //Check email Empty
        if (paramOrderObj.email === "") {
            vResult = false;
            vMessage += "Please type Email !" + " or ";
        }
        //Check @ in Email
        if (!validateEmail(paramOrderObj.email)) {
            vResult = false;
            vMessage += "Email input is not an Email. Please to fix that !!!" + "\n";
        }
        //Check paramOrderObj Empty
        if (paramOrderObj.fone === "") {
            vResult = false;
            vMessage += "Please type Fone !" + " or ";
        }
        // chuyển giá trị fone sang dạng số, hệ cơ số 10
        var vFoneNumber = parseInt(paramOrderObj.fone, 10);
        // Check : Input fone is a number ?
        if (isNaN(vFoneNumber)) {
            vResult = false;
            vMessage += "Fone input is not a number. Please to fix that !!!" + "\n";
        }
        //Check address Empty
        if (paramOrderObj.address === "") {
            vResult = false;
            vMessage += "Please type Address !" + "\n";
        }
        if (vResult === false) {
            alert(vMessage)
        };
        return vResult;
    } // function validateOrder end

    // function validateemail
    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    } // function validateemail end

    // function loadAjaxAPIGetVoucher gọi api lấy Voucher percent by Voucher Id
    function loadAjaxAPIGetVoucher() {
        "use strict";
        $.ajax({
            url: gBASE_VOUCHER_URL + gOrder.voucher,
            type: 'GET',
            dataType: 'json', // added data type
            success: function(response) {
                gOrder.percentDiscount = response.phanTramGiamGia;
                // mở modal Tạo đơn
                $("#order-modal").modal("show");
                displayDataOrderConfirm();
            },
            error: function(ajaxContext) {
                gOrder.percentDiscount = 0;
                // mở modal Tạo đơn
                $("#order-modal").modal("show");
                displayDataOrderConfirm();
            }
        });
    } // function loadAjaxAPIGetVoucher gọi api lấy Voucher percent by Voucher Id

    // Function hiện ra màn hình các dữ liệu form xác nhận
    function displayDataOrderConfirm() {
        "use strict";
        // Bước 1: Truy xuất dữ liệu 
        // console.log(gNewOrderObject);
        // hiển thị vùng div subject lên
        // $("#div-container-order").show();
        // lấy giá trị voucher
        if (gOrder.voucher !== null) {} else {
            gOrder.percentDiscount = 0;
            alert("Can't Find this Voucher: " + gOrder.voucher + " ! => percentDiscount = 0 ! Please type another Voucher !");
        }
        getTotalPayment(gSelectedMenuStructure.priceVND, gOrder.percentDiscount);
        // ghi dữ liệu other vào modal
        $("#inp-fullname-send").val(gOrder.fullname).prop('disabled', true);
        $("#inp-fone-send").val(gOrder.fone).prop('disabled', true);
        $("#inp-address-send").val(gOrder.address).prop('disabled', true);
        $("#inp-message-send").val(gOrder.message).prop('disabled', true);
        $("#inp-voucher-send").val(gOrder.voucher).prop('disabled', true);
        $("#inp-detail-order-send")
            .css("height", "150px")
            .prop('disabled', true)
            .html("Xác nhận: " + gOrder.fullname + ", " + gOrder.fone + ", " + gOrder.address + '\n' +
                "Menu: " + gSelectedMenuStructure.menuName +
                ", đường kính: " + gSelectedMenuStructure.duongKinhCM + "cm" +
                ", sườn nướng: " + gSelectedMenuStructure.suongNuong + " miếng" +
                ", Salad: " + gSelectedMenuStructure.saladGr + "g" + '\n' +
                "Nước uống: " + gSelectedDrink.text +
                ", số lượng: " + gSelectedMenuStructure.drink + '\n' +
                "Loại pizza: " + gSelectedPizzaType +
                " Giá: " + gSelectedMenuStructure.priceVND.toLocaleString() + " VNĐ" +
                ", Mã giảm giá: " + gOrder.voucher + '\n' +
                "Phải thanh toán: " + gOrder.total.toLocaleString() + " VNĐ (giảm giá: " + gOrder.percentDiscount + "%)"
            );
        // console.log(gNewOrderObject);
    } //function displayDataOrderConfirm end

    function getTotalPayment(paramPrice, paramPercentDiscount) {
        gOrder.total = paramPrice * (100 - paramPercentDiscount) / 100;
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

    // Hàm onBtnConfirmClick khi click vào button gửi dữ liệu Create new Order lên Server 
    // chức năng: xác nhận đơn hàng và gửi dữ liệu API => tạo mới đơn hàng
    $("#btn-create-order").on("click", function() {
        "use strict";
        $("#order-modal").modal("hide");
        $("#result-modal").modal("show");
        // Bước 1: Truy xuất dữ liệu 
        getDataSendForm();
        // Bước 2: Validate dữ liệu 
        // Bước 3: Call API 
        callAPICreateNewOrder();
        // ghi dữ liệu other vào thẻ div subject info
        $("#div-order-infor-confirm").html(gThankYou + "<br>");
    }); // function onBtnConfirmClick end

    // hàm thực hiện việc gửi dữ liệu New Oder đến server
    function callAPICreateNewOrder() {
        "use strict";
        $.ajax({
            url: gBASE_URL + "/orders",
            type: 'POST',
            data: JSON.stringify(gNewOrderObject),
            contentType: gUTF8_TEXT_APPLICATION_HEADER,
            success: function(responseNewOrder) {
                // gNewOrderObject = Object.entries(responseNewOrder)
                gNewOrderObject = responseNewOrder;
                console.log(gNewOrderObject);
                handleCreateNewOrderResponse(responseNewOrder);
            },
            error: function(ajaxContext) {
                alert(ajaxContext.responseText)
                console.log("2");
            }
        });

    } // function callAPICreateNewOder end
    // hàm thực hiện việc xử lý dữ liệu Create New Oder API trả về
    function handleCreateNewOrderResponse(paramNewOrderObj) {
        "use strict";
        // gán giá trị trả về từ server vào biến toàn cục gNewOrderObject
        gNewOrderObject.id = paramNewOrderObj.id;
        gNewOrderObject.orderId = paramNewOrderObj.orderId;
        gNewOrderObject.kichCo = paramNewOrderObj.kichCo;
        gNewOrderObject.duongKinh = paramNewOrderObj.duongKinh;
        gNewOrderObject.suon = paramNewOrderObj.suon;
        gNewOrderObject.salad = paramNewOrderObj.salad;
        gNewOrderObject.loaiPizza = paramNewOrderObj.loaiPizza;
        gNewOrderObject.idVourcher = paramNewOrderObj.idVourcher;
        gNewOrderObject.idLoaiNuocUong = paramNewOrderObj.idLoaiNuocUong;
        gNewOrderObject.soLuongNuoc = paramNewOrderObj.soLuongNuoc;
        gNewOrderObject.hoTen = paramNewOrderObj.hoTen;
        gNewOrderObject.thanhTien = paramNewOrderObj.thanhTien;
        gNewOrderObject.email = paramNewOrderObj.email;
        gNewOrderObject.soDienThoai = paramNewOrderObj.soDienThoai;
        gNewOrderObject.diaChi = paramNewOrderObj.diaChi;
        gNewOrderObject.loiNhan = paramNewOrderObj.loiNhan;
        gNewOrderObject.trangThai = paramNewOrderObj.trangThai;
        gNewOrderObject.ngayTao = paramNewOrderObj.ngayTao;
        gNewOrderObject.ngayCapNhat = paramNewOrderObj.ngayCapNhat;
        // console.log(gNewOrderObject);
        // console.log(gNewOrderObject.orderId);
        $("#inp-orderid").val(gNewOrderObject.orderId);
    } //function handleCreateNewOderResponse end



    // hàm truy xuất các dữ liệu để gửi request đến Server
    function getDataSendForm() {
        "use strict";
        // Bước 1: Truy xuất dữ liệu 
        // ghi dữ liệu other vào thẻ div subject info
        gNewOrderObject.kichCo = gSelectedMenuStructure.menuName;
        gNewOrderObject.duongKinh = gSelectedMenuStructure.duongKinhCM;
        gNewOrderObject.suon = gSelectedMenuStructure.suongNuong;
        gNewOrderObject.salad = gSelectedMenuStructure.saladGr
        gNewOrderObject.loaiPizza = gSelectedPizzaType;
        gNewOrderObject.idVourcher = gOrder.voucher;
        gNewOrderObject.idLoaiNuocUong = gSelectedDrink.value;
        gNewOrderObject.soLuongNuoc = gSelectedMenuStructure.drink
        gNewOrderObject.hoTen = gOrder.fullname;
        gNewOrderObject.thanhTien = gOrder.total;
        gNewOrderObject.email = gOrder.email;
        gNewOrderObject.soDienThoai = gOrder.fone
        gNewOrderObject.diaChi = gOrder.address;
        gNewOrderObject.loiNhan = gOrder.message;
        // Bước 2: Validate dữ liệu pass
        // Bước 3: 
    } // function getDataSendForm end


}); // $(document).ready(function() end